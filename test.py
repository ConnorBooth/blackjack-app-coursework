
#test for shuffling cards

import random #import the random function

r = [0,1,2,3,4,5,6,7,8,9,10,11,12,13] #list of cards to be shuffled in game

print ("The original list is : " + str(r)) #line of ascending numbers should be printed

random.shuffle(r) #used random.shuffle function to shuffle cards
   
print ("The shuffled list is : " +  str(r)) #An unorganised list will be produced
